package Homework13.linked;

public class LinkedList {
    Node first;
    private Node last;

    private static int staticvalue;

    private int count;

    public LinkedList() {
        count = 0;
    }

    // nested (static nested)
    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }

        int getValue() {
            return value;
        }

        void setValue(int value) {
            this.value = value;
        }

        Node getNext() {
            return next;
        }

        void setNext(Node next) {
            this.next = next;
        }

    }

    // inner (non-static nested)
    class LinkedListIterator {
        Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public int next() {
            int value = current.value;
            current = current.next;
            return value;
        }
    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return -1;
        }
    }

    public void addToBegin(int element) {
        Node newNode = new Node(element);
        newNode.setNext(first);
        first = newNode;
        count++;
    }

    public void remove(int element) {
        int i = 0;
        LinkedListIterator iterator = iterator();
        while (iterator.hasNext()) {
            if(iterator.current.value == element) {
                removeByIndex(i);
            }
            iterator.next();
            i++;
        }
    }

    public void removeByIndex(int index) {
        if(index == 0) {
            first = first.next;
        } else {
            Node current = first;
            for (int i = 0; i < index -1 ; i++) {
                current = current.next;
            }
            current.next = current.next.next;
        }
    }

    public boolean contains(int element) {
        LinkedListIterator iterator = iterator();
        while (iterator.hasNext()) {
            if(iterator.current.value == element) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return count;
    }

    public LinkedListIterator iterator() {
        return new LinkedListIterator();
    }
}

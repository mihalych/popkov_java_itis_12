package Homework13.linked;

public class MainForLinkedList {

    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.add(3);
        list.add(7);
        list.add(8);
        list.add(9);

        LinkedList.LinkedListIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println();

        list.addToBegin(5);
        LinkedList.LinkedListIterator iterator1 = list.iterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }
        System.out.println();

        list.removeByIndex(3);

        LinkedList.LinkedListIterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }

        System.out.println();

        list.remove(3);

        LinkedList.LinkedListIterator iterator3 = list.iterator();
        while (iterator3.hasNext()) {
            System.out.println(iterator3.next());
        }
    }
}

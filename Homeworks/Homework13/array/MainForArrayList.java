package Homework13.array;

public class MainForArrayList {

    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(10);
        list.add(5);
        list.add(7);
        list.add(77);
        list.add(7);
        list.add(7);
        list.add(7);
        list.add(7);
        list.add(7);
        list.add(7);
        list.add(7);

        list.addToBegin(100);

        ArrayList.ArrayListIterator iterator1 = list.iterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }

        System.out.println();

        list.remove(7);

        ArrayList.ArrayListIterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }

        System.out.println();

        list.removeByIndex(3);

        ArrayList.ArrayListIterator iterator3 = list.iterator();
        while (iterator3.hasNext()) {
            System.out.println(iterator3.next());
        }

    }
}

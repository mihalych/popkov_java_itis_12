package Homework13.array;

public class ArrayList {
    private static final int DEFAULT_ARRAY_SIZE = 10;

    private int elements[];
    private int count;

    public ArrayList() {
        this.elements = new int[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    public ArrayListIterator iterator() {
        return new ArrayListIterator();
    }

    class ArrayListIterator {

        int cursor;

        public ArrayListIterator() {
            cursor = 0;
        }

        public boolean hasNext() {
            return cursor < count;
        }

        public int next() {
            int value = elements[cursor];
            cursor++;
            return value;
        }

    }

    public void add(int element) {
        if (count < elements.length) {
            this.elements[count] = element;
            this.count++;
        } else {
            increaseArray();
            add(element);
        }
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.err.println("Неверный индекс");
            return -1;
        }
    }

    public void addToBegin(int element) {
        if (count < elements.length) {
            for (int i = count; i > 0; i--) {
                elements[i] = elements[i - 1];
            }
            this.elements[0] = element;
            this.count++;
        } else {
            increaseArray();
            addToBegin(element);
        }
    }

    public void remove(int element) {
        for (int i = 0; i < count; i++) {
            if (element == elements[i]) {
                removeByIndex(i);
                i--;
            }
        }
    }

    public void removeByIndex(int index) {
        if (index >= 0 && index < count) {
            for (int i = index; i <= count; i++) {
                elements[i] = elements[i + 1];
            }
            count--;
        } else {
            System.err.println("Неверный индекс");
        }
    }

    public boolean contains(int element) {
        for (int i = 0; i < count; i++) {
            if (element == elements[i]) {
                return true;
            }
        }
        return false;
    }

    public void increaseArray() {
        int newSize = elements.length + elements.length / 2;
        int[] newArray = new int[newSize];
        for (int i = 0; i < elements.length; i++) {
            newArray[i] = elements[i];
        }
        elements = newArray;
    }

    public int size() {
        return count;
    }
}

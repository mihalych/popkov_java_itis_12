import java.util.Scanner;

class Homework5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int values[] = new int[8];

		for (int i = 0; i < values.length; i++) {
			values[i] = scanner.nextInt();
		}

		for (int i = values.length-1; i >= 0; i--) {
			System.out.println(values[i]);
		}

	}
}
class Homework10 {
	public static void main(String[] args) {
		char n[] = {'3', '2', '4'};
		int x = parseInt(n);
		System.out.println(x);
	}
	public static int parseInt(char[]n) {
		int number = 0;
        for (int i = 0; i < n.length; i++) {
            int b = n.length - i;
            int ten = 1;
            while (b > 1) {
                ten = ten *10;
                b--;
            }
            number += (n[i] - 48) * ten;
        }
        return number;
	}
}
package Homework16;

public interface List<V> {
    V get(int index);
    void add(V object);
    void addToBegin(V object);
    void remove(V object);
    void removeByIndex(int index);
    boolean contains(V object);
    int size();
}

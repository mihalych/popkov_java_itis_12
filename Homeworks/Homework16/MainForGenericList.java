package Homework16;

public class MainForGenericList {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();

        list.add(3);
        list.add(7);
        list.add(8);
        list.add(9);

        LinkedList.LinkedListIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println();

        list.addToBegin(5);
        LinkedList.LinkedListIterator iterator1 = list.iterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }
        System.out.println();

        list.removeByIndex(3);

        LinkedList.LinkedListIterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }

        System.out.println();

        list.remove(3);

        LinkedList.LinkedListIterator iterator3 = list.iterator();
        while (iterator3.hasNext()) {
            System.out.println(iterator3.next());
        }

        /////////////////////////////////////////////////////////////

        LinkedList<String> listString = new LinkedList<>();

        listString.add("First");
        listString.add("Second");
        listString.add("Third");
        listString.add("Fourth");

        LinkedList.LinkedListIterator iteratorString = listString.iterator();
        while (iteratorString.hasNext()) {
            System.out.println(iteratorString.next());
        }
        System.out.println();

        listString.addToBegin("Fifth");
        LinkedList.LinkedListIterator iteratorString1 = listString.iterator();
        while (iteratorString1.hasNext()) {
            System.out.println(iteratorString1.next());
        }
        System.out.println();

        listString.removeByIndex(3);

        LinkedList.LinkedListIterator iteratorString2 = listString.iterator();
        while (iteratorString2.hasNext()) {
            System.out.println(iteratorString2.next());
        }

        System.out.println();

        listString.remove("Second");

        LinkedList.LinkedListIterator iteratorString3 = listString.iterator();
        while (iteratorString3.hasNext()) {
            System.out.println(iteratorString3.next());
        }
    }
}

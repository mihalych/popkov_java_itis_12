package Homework16;

public class LinkedList<V> implements List<V> {

    Node first;
    private Node last;

    private int count;

    public LinkedList() {
        count = 0;
    }

    private class Node {
        V value;
        Node next;

        Node(V value) {
            this.value = value;
        }

        V getValue() {
            return value;
        }

        void setValue(V value) {
            this.value = value;
        }

        Node getNext() {
            return next;
        }

        void setNext(Node next) {
            this.next = next;
        }

    }

    class LinkedListIterator {
        Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public V next() {
            V value = current.value;
            current = current.next;
            return value;
        }
    }

    public LinkedListIterator iterator() {
        return new LinkedListIterator();
    }

    @Override
    public V get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return null;
        }
    }

    @Override
    public void add(V object) {
        Node newNode = new Node(object);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public void addToBegin(V object) {
        Node newNode = new Node(object);
        newNode.setNext(first);
        first = newNode;
        count++;
    }

    @Override
    public void remove(V object) {
        int i = 0;
        LinkedListIterator iterator = iterator();
        while (iterator.hasNext()) {
            if(iterator.current.value.equals(object)) {
                removeByIndex(i);
            }
            iterator.next();
            i++;
        }
    }

    @Override
    public void removeByIndex(int index) {
        if(index == 0) {
            first = first.next;
        } else {
            Node current = first;
            for (int i = 0; i < index -1 ; i++) {
                current = current.next;
            }
            current.next = current.next.next;
        }
    }

    @Override
    public boolean contains(V object) {
        LinkedListIterator iterator = iterator();
        while (iterator.hasNext()) {
            if(iterator.current.value.equals(object)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return this.count;
    }
}

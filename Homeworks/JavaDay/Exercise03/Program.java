package JavaDay.Exercise03;

import java.util.*;

public class Program {
    public static void main(String[] args) {
        Map<String, ArrayList<Integer>> weeks = new HashMap<String, ArrayList<Integer>>();
        Scanner scanner = new Scanner(System.in);
        int weekCount = 0;
        while (scanner.hasNext()) {

            String line = scanner.nextLine();
            int n = 0;
            if (line.startsWith("Week")) {
                String key = line;
                weeks.put(key, new ArrayList());
                for (int i = 0; i < 5; i++) {
                    n = scanner.nextInt();
                    weeks.get(key).add(n);
                }
                weekCount++;
            } else if (line.equals("42")) {
                scanner.close();
                break;
            }
        }
        int m = 0;
        if (weekCount > 0) {
            for(Map.Entry<String, ArrayList<Integer>> mapEntry : weeks.entrySet()) {
                sort(mapEntry.getValue());
                System.out.print(mapEntry.getKey() + " ");
                for(int i = 0; i < mapEntry.getValue().get(0); i++) {

                    System.out.printf("=");

                }
                System.out.println(">");
            }
        }
    }

    public static void sort(ArrayList<Integer> a) {
        Collections.sort(a);
    }
}

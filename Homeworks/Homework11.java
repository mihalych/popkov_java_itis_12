import java.util.Scanner;

class Homework11 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
        char text[] = scanner.nextLine().toCharArray();
        parseChar(text);
	}

	public static void parseChar(char[] n) {

        int count = 0;
        int[] charCount = new int[26];

        for (int i = n.length - 1; i >= 0; i--) {
            char ch = n[i];
            if (isLowerEnglishLetter(ch)) {
                ch = (char) ((int) n[i] - 32);
            }
            int index = (int) (ch - 65);
            if (index >= 0 && index < charCount.length) {
                charCount[index]++;
                if (charCount[index] >= count) {
                    count = charCount[index];
                }
            }
        }
        for (int i = 0; i < charCount.length; i++) {
            if (count == charCount[i]) {
                char currentChar = (char) (i + 65);
                System.out.println(currentChar + " - " + count);
            }
        }
    }

    public static boolean isLowerEnglishLetter(char c) {
        return c >= 'a' && c <= 'z';
    }
}
package clientSocket;

import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        clientSocket client = new clientSocket(arguments.serverHost, arguments.serverPort);

        while (true) {
            String message = scanner.nextLine();
            client.sendMessage(message);
        }
    }
}

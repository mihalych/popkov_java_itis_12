class Homework7 {
	public static void main(String[] args) {

		int digitSum = digitSum(56978);
		System.out.println(digitSum);

		int mirrorNumber = mirrorNumber(56978);
		System.out.println(mirrorNumber);

		int sumBetween = sumBetween(10,13);
		System.out.println(sumBetween);
	}

	public static int digitSum(int number) {
		int sum = 0;
		while (number > 0) {
			sum = sum + number % 10;
			number = number / 10;
		}

		return sum;
	}

	public static int mirrorNumber(int number) {
		
		int result = 0;
		int mirrorNumber = 0;
		int numberCounter = number;
		int counter = 1;

		while (numberCounter > 10) {
			numberCounter = numberCounter / 10;
			counter = counter * 10;
		}
		while (number > 0) {
			mirrorNumber = number % 10 * counter;
			result = result + mirrorNumber;			
			counter = counter / 10;
			number = number / 10;
		}
		return result;
	}
	public static int sumBetween(int a, int b) {
		int resultSum = 0;
		for (int i = a + 1; i < b; i++) {
			a = a + 1;
			resultSum = resultSum + a;
		}
		return resultSum;
	}
}
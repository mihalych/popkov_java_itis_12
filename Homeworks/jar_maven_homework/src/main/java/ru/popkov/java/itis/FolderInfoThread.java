package ru.popkov.java.itis;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FolderInfoThread implements Runnable{

    private String folderPath;
    private Map<String, List<FileInformation>> folderInfoMap;

    public FolderInfoThread(String folderPath, Map<String, List<FileInformation>> folderInfoMap) {
        this.folderPath = folderPath;
        this.folderInfoMap = folderInfoMap;
    }

    public void run() {

        File folder = new File(folderPath);
        String folderName = folder.getName();

        if (!folder.isFile()) {
            List<FileInformation> fileInfpList = new LinkedList<FileInformation>();
            for (File file : folder.listFiles()) {
                FileInformation fileInfo = new FileInformation(file.getName(), !file.isFile());
                fileInfpList.add(fileInfo);
                System.out.println(folderName + " " + file.getName());
            }
            folderInfoMap.put(folderName, fileInfpList);
        }
    }
}

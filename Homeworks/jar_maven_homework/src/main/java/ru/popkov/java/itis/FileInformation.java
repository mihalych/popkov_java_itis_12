package ru.popkov.java.itis;

public class FileInformation {

    private boolean isFolder;
    private String fileName;

    public FileInformation(String fileName, Boolean isFolder) {

        this.fileName = fileName;
        this.isFolder = isFolder;

    }

    public String fileName() {
        return fileName;
    }

    public boolean isFolder() {
        return isFolder;
    }
}

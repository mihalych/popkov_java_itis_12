package ru.popkov.java.itis;

import java.util.*;

class Main {
    public static void main(String[] args) throws InterruptedException {
        Map<String, List<FileInformation>> folderInfoMap = new HashMap<String, List<FileInformation>>();
        for(int i = 0; i < args.length; i++) {
            String folderPath = args[i];
            FolderInfoThread folderInfoThread = new FolderInfoThread(folderPath, folderInfoMap);
            Thread thread = new Thread(folderInfoThread);
            thread.start();
            thread.join();
        }
        int b = 1;
    }
}

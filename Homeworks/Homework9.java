import java.util.Scanner;

class Homework9 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int resultIndex = scanner.nextInt();
		int curIndex = 0;
        int curNumber = 0;
        int nextNumber = 1;
		int a = fibonacci(resultIndex, curIndex, curNumber, nextNumber);
		System.out.println(a);
	}

	public static int fibonacci(int resultIndex, int curIndex, int curNumber, int nextNumber) {
		if (resultIndex == 0) return 0;
		nextNumber = curNumber + nextNumber;
        curNumber = nextNumber - curNumber;
        curIndex++;
        if (resultIndex == curIndex) return curNumber;
        curNumber = fibonacci(resultIndex, curIndex, curNumber, nextNumber);
        return curNumber;
	}
}
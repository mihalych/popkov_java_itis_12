package ru.itdrive.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

@Component
public class UsersRepository {

    //language=SQL
    private static final String SQL_SELECT_USER = "select * from users where name = ? and password = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from users where id = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from users";

    //language=SQL
    private static final String SQL_SAVE_USER = "insert into users(name, password) values (?, ?)";

    //language=SQL
    private static final String SQL_SAVE_COOKIE = "insert into users_cookies(user_id, cookie) values (?, ?)";

    //language=SQL
    private static final String SQL_SELECT_USER_TOKEN = "select * from users_cookies where user_id = ?";

    //language=SQL
    private static final String SQL_SAVE_FILE = "insert into users_files(token, file_name) values (?, ?)";

    //language=SQL
    private static final String SQL_SELECT_USER_FILES = "select * from users_files where token = ?";

    private DataSource dataSource;

    public UsersRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {
            return new User(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("password"));
        }
    };

    public User findUser(User user) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER);
            statement.setString(1, user.getName());
            statement.setString(2, user.getPassword());

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            return userRowMapper.mapRow(resultSet);

        } catch (SQLException e) {
            return null;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }

    public void saveCookie(User user, String token) {

        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SAVE_COOKIE);
            statement.setInt(1, user.getId());
            statement.setString(2, token);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }

    public String findUserToken(User user) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER_TOKEN);
            statement.setInt(1, user.getId());

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            return resultSet.getString("cookie");

        } catch (SQLException e) {
            return null;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }

    public void saveFileWithCookie(String token, String fileName) {

        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SAVE_FILE);
            statement.setString(1, token);
            statement.setString(2, fileName);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }

    public ArrayList<String> findUserFiles(String token) {
        PreparedStatement statement = null;
        ArrayList<String> filesList = new ArrayList<String>();
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER_FILES);
            statement.setString(1, token);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                filesList.add(resultSet.getString("file_name"));
            }
            return filesList;
        } catch (SQLException e) {
            return null;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }
}

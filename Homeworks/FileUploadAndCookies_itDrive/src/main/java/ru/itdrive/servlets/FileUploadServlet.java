package ru.itdrive.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.repositories.UsersRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@WebServlet("/fileUpload")
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

    private UsersRepository usersRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/fileUpload.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("userToken")) {

                Part part = request.getPart("file");
                String fileName = UUID.randomUUID().toString();

                Files.copy(part.getInputStream(), Paths.get("C://Projects//popkov_java_itis_12//Homeworks//FileUploadAndCookies_itDrive/files/" + fileName));
                usersRepository.saveFileWithCookie(cookie.getValue(), fileName);
                response.sendRedirect("/filesList");
                response.addCookie(cookie);
            }
        }
    }

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        usersRepository = context.getBean(UsersRepository.class);
    }
}

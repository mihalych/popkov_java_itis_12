package ru.itdrive.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.models.User;
import ru.itdrive.repositories.UsersRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private UsersRepository usersRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        usersRepository = context.getBean(UsersRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("html/signIn.html");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        User user = User.builder()
                .name(name)
                .password(password)
                .build();

        User findUser = usersRepository.findUser(user);

        if (findUser != null) {
            String token = usersRepository.findUserToken(findUser);
            if (token == null) {
                token = UUID.randomUUID().toString();
                usersRepository.saveCookie(findUser, token);
            }
            Cookie cookie = new Cookie("userToken", token);
            response.addCookie(cookie);
            response.sendRedirect("/fileUpload");
        } else {
            response.sendRedirect("/signIn");
        }
    }
}

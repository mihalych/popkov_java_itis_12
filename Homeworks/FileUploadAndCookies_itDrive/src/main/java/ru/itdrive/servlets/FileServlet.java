package ru.itdrive.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@WebServlet("/files")
public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");

        File file = new File("C://Projects//popkov_java_itis_12//Homeworks//FileUploadAndCookies_itDrive/files/" + id);
        response.setContentType("image/jpeg");
        response.setContentLength((int)file.length());
        response.setHeader("Content-Disposition", "filename=\"" + id + "\"");
        Files.copy(file.toPath(), response.getOutputStream());
        response.flushBuffer();
    }
}

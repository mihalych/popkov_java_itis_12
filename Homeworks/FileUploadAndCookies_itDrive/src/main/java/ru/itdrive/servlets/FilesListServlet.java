package ru.itdrive.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.repositories.UsersRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/filesList")
public class FilesListServlet extends HttpServlet {

    private UsersRepository usersRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();
        Cookie[] cookies = request.getCookies();
        ArrayList<String> filesList = new ArrayList<String>();

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("userToken")) {
                filesList = usersRepository.findUserFiles(cookie.getValue());
            }
        }

        StringBuilder s = new StringBuilder();

        for (int i = 0; i < filesList.size(); i++) {
            String url = "http://localhost:8080/files?id=" + filesList.get(i).trim();
            s.append("<a href='").append(url).append("'>").append(url).append("</a>\n");
        }
        writer.println("<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <title>Files list</title>\n" +
                "</head>\n" +
                "<body>\n" +
                s + "\n" +
                "</body>\n" +
                "</html>");

    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        usersRepository = context.getBean(UsersRepository.class);
    }

}

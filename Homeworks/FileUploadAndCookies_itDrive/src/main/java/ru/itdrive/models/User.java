package ru.itdrive.models;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@Data
@Builder
public class User {
    private Integer id;
    private String name;
    private String password;
}

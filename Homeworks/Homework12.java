public class Homework12 {
    public static void main(String[] args) {

        int ageArray[] = {45, 32, 36, 57, 45, 12, 7, 56, 23, 48, 69, 89, 2};
        int maxAge = getMaxAge(ageArray);
        System.out.println(maxAge);
    }

    static int getMaxAge(int ageArray[]) {

        int maxAge = 0;

        for (int i = 0; i < ageArray.length; i++) {
            if(ageArray[i] > maxAge) maxAge = ageArray[i];
        }
        return maxAge;
    }
}

package Homework14;

import java.util.Random;

public class MainForTV {
    public static void main(String[] args) {

        Random r = new Random();
        int channelCount = r.nextInt(75);
        channelCount = channelCount == 0 ? 1 : channelCount;

        TV tv = new TV(channelCount);

        RemoteController remoteController = new RemoteController(tv);
        Broadcast currentBroadcast = remoteController.click(5);

        System.out.println(currentBroadcast.getStartTime() + " - " + currentBroadcast.getEndTime());

    }
}

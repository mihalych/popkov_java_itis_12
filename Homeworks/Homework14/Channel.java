package Homework14;

import java.time.LocalTime;
import java.util.Random;

public class Channel {

    private Broadcast[] broadcastList;

    private int channelNumber = 0;

    public Channel(int channelNumber) {

        Random r = new Random();
        int broadcastCount = r.nextInt(40);
        broadcastCount = broadcastCount == 0 ? 1 : broadcastCount;

        int broadcastAvaragetMinute = 1440 / broadcastCount;
        int broadcastMinute = 0;
        broadcastList = new Broadcast[broadcastCount];

        this.channelNumber = channelNumber;

        for (int i = 0; i < broadcastList.length; i++) {
            LocalTime time = LocalTime.MIN.plusMinutes(broadcastMinute);
            broadcastList[i] = new Broadcast(time, broadcastAvaragetMinute);
            broadcastMinute += broadcastAvaragetMinute;
        }
    }

    public Broadcast getBroadcastAtCurrentTime() {
        LocalTime currentTime = LocalTime.now();
        for (Broadcast broadcast : broadcastList) {

            if(broadcast.getEndTime().isAfter(currentTime) && broadcast.getStartTime().isBefore(currentTime)) {
                return broadcast;
            }
        }
        return null;
    }

}

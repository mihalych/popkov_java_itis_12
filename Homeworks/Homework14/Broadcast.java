package Homework14;

import java.time.LocalTime;
import java.util.Random;

public class Broadcast {
    private LocalTime startTime;
    private LocalTime endTime;

    public Broadcast(LocalTime time, int broadcastAvaragetMinute) {

        this.startTime = time;
        this.endTime = time.plusMinutes(broadcastAvaragetMinute);

    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }
}

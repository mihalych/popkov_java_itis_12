package Homework14;

public class TV {

    private Channel channelArray[];
    private int channelCount;

    public TV(int channelCount) {
        this.channelCount = channelCount;
        channelArray = new Channel[this.channelCount];
        for (int i = 0; i < channelArray.length; i++) {
            channelArray[i] = new Channel(i + 1);
        }
    }

    public Channel getChannel(int channelNumber) {
        if(channelNumber > channelCount || channelNumber < 1) {
            System.out.println("Incorrect number, number must be between 1 and " + channelCount);
            return null;
        }
        Channel channel = channelArray[channelNumber-1];
        return channel;
    }
}

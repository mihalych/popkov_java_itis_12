package Homework14;

public class RemoteController {

    private TV tv;

    public RemoteController(TV tv) {
        this.tv = tv;
    }

    public Broadcast click(int channelNumber) {
        Channel channel = this.tv.getChannel(channelNumber);

        Broadcast broadcast  = channel.getBroadcastAtCurrentTime();
        if (broadcast != null) return broadcast;
        return null;
    }

}

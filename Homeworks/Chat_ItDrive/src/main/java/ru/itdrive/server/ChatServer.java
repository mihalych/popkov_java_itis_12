package ru.itdrive.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.services.MessagesService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import java.util.*;

@Component
public class ChatServer {

    @Autowired
    private int port;

    @Autowired
    private MessagesService messagesService;

    private List<ChatSocketClient> anonymus;
    private Map<Integer, List<ChatSocketClient>> users;

    public ChatServer(int port, MessagesService messagesService) {
        this.port = port;
        this.messagesService = messagesService;
        users = new HashMap<Integer, List<ChatSocketClient>>();
        anonymus = new ArrayList<ChatSocketClient>();
    }

    private class ChatSocketClient extends Thread {

        public ChatSocketClient(Socket socket) {
            this.socket = socket;
        }

        private Integer userId;

        public Integer getUserId() {
            return userId;
        }

        public Integer getRoomId() {
            return roomId;
        }

        private Integer roomId;
        private Socket socket;

        public Socket getSocket() {
            return socket;
        }

        private BufferedReader input;
        private PrintWriter output;

        @Override
        public void run() {
            try {

                Iterator roomIdIterator = messagesService.getListOfRoomId().iterator();
                while(roomIdIterator.hasNext()) {
                    users.put((Integer) roomIdIterator.next(), new ArrayList<ChatSocketClient>());
                }
                anonymus.add(this);

                InputStream inputStream = socket.getInputStream();
                input = new BufferedReader(new InputStreamReader(inputStream));
                output = new PrintWriter(socket.getOutputStream(), true);
                output.println("Hello in chat bot. Enter command 'login'");

                String inputLine = input.readLine();

                while (inputLine != null) {

                    //login
                    if (messagesService.isCommandLogin(inputLine)) {
                        output.println("Enter email");
                        inputLine = input.readLine();

                        userId = messagesService.getUserIdByEmail(inputLine);

                        if(userId != null) {
                            output.println("login successful");
                            anonymus.remove(this);
                            roomId = messagesService.getUserLastRoom(userId);
                            if(roomId != null) {
                                List<String> messagesList = messagesService.getUserLastMessagesInRoom(userId, roomId);

                                for (int i = 0; i < messagesList.size(); i++ ) {
                                    output.println(messagesList.get(i));
                                }
                            }
                        } else {
                            output.println("login unsuccessful");
                        }
                        inputLine = input.readLine();
                    }

                    //choose room
                    if (messagesService.isCommandChooseRoom(inputLine) & userId != null & roomId == null) {
                        output.println("Enter room id");
                        inputLine = input.readLine();

                        roomId = messagesService.getRoomId(Integer.valueOf(inputLine));

                        if(roomId != null) {
                            users.get(roomId).add(this);
                            output.println("You in room " + roomId);
                        } else {
                            output.println("Room not find");
                        }
                        inputLine = input.readLine();
                    }

                    //exit
                    if (messagesService.isCommandExit(inputLine) & userId != null & roomId != null) {
                        messagesService.saveUserLastRoom(userId,roomId);
                        inputLine = input.readLine();
                    }

                    if (roomId != null) {
                        List<ChatSocketClient> usersInRoom = users.get(roomId);
                        for (int i = 0; i < usersInRoom.size(); i++ ) {

                            messagesService.saveMessage(inputLine, roomId, userId);
                            PrintWriter currentOutput = new PrintWriter(usersInRoom.get(i).getSocket().getOutputStream(), true);
                            currentOutput.println(inputLine);
                        }
                    }
                    inputLine = input.readLine();
                }
            } catch (IOException e) {

            }
        }
    }

    public void start() {

        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(port);
            while(true) {
                new ChatSocketClient(serverSocket.accept()).start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

package ru.itdrive.services;

import ru.itdrive.model.Message;
import ru.itdrive.model.Room;
import org.springframework.stereotype.Component;
import ru.itdrive.model.User;
import ru.itdrive.model.UserLastRoom;
import ru.itdrive.repositories.*;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class MessagesService {

    private static final String commandChooseRoom = "choose room";
    private static final String commandLogin = "login";
    private static final String commandExit = "exit";

    private DataSource dataSource;

    public MessagesService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void saveMessage(String messageString, Integer chatId, Integer authorId) {

        Message message = new Message(null,messageString, chatId, authorId, new Date(System.currentTimeMillis()));
        MessagesRepositoryImpl messagesRepository = new MessagesRepositoryImpl(dataSource);
        messagesRepository.save(message);
    }

    public Integer getUserIdByEmail(String email) {
        UsersRepositoryImpl usersRepository = new UsersRepositoryImpl(dataSource);
        User user = usersRepository.findByEmail(email);
        if (user != null) {
            return user.getId();
        }
        return null;
    }

    public Integer getUserLastRoom(Integer userId) {

        UserLastRoomRepository userLastRoomRepository = new UserLastRoomRepositoryImpl(dataSource);
        UserLastRoom userLastRoom = userLastRoomRepository.findUserLastRoom(userId);
        if (userLastRoom != null) {
            return userLastRoom.getRoomId();
        } else {
            return null;
        }
    }

    public List<String> getUserLastMessagesInRoom(Integer userId, Integer roomId) {

        List<String> messagesList = new ArrayList<String>();

        MessagesRepositoryImpl messagesRepository = new MessagesRepositoryImpl(dataSource);
        List<Message> messages = messagesRepository.findMessagesByRoomAndAuthor(roomId, userId);
        Iterator<Message> messageIterator = messages.iterator();
        while (messageIterator.hasNext()) {
            messagesList.add(messageIterator.next().getText());
        }
        return messagesList;
    }

    public void saveUserLastRoom(Integer userId, Integer roomId) {

        UserLastRoomRepository userLastRoomRepository = new UserLastRoomRepositoryImpl(dataSource);
        UserLastRoom userLastRoom = userLastRoomRepository.findUserLastRoom(userId);
        if (userLastRoom != null) {
            userLastRoom.setRoomId(roomId);
            userLastRoomRepository.update(userLastRoom);
        } else {
            userLastRoom = new UserLastRoom(null, userId, roomId);
            userLastRoomRepository.save(userLastRoom);
        }
    }

    public List<Integer> getListOfRoomId() {

        List<Integer> listOfRoomId= new ArrayList<Integer>();
        RoomRepositoryImpl roomRepository = new RoomRepositoryImpl(dataSource);
        Iterator<Room> roomIterator= roomRepository.findAll().iterator();
        while (roomIterator.hasNext()) {
            listOfRoomId.add(roomIterator.next().getId());
        }
        return listOfRoomId;
    }

    public Integer getRoomId(int id) {

        RoomRepositoryImpl roomRepository = new RoomRepositoryImpl(dataSource);
        return roomRepository.find(id).getId();
    }

    public boolean isCommandChooseRoom(String text) {
        CharSequence charSequence = commandChooseRoom.subSequence(0,commandChooseRoom.length());
        return text.contains(charSequence);
    }

    public boolean isCommandLogin(String text) {
        CharSequence charSequence = commandLogin.subSequence(0,commandLogin.length());
        return text.contains(charSequence);
    }

    public boolean isCommandExit(String text) {
        CharSequence charSequence = commandExit.subSequence(0,commandExit.length());
        return text.contains(charSequence);
    }
}
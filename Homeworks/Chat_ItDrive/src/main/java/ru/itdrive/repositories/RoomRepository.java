package ru.itdrive.repositories;

import ru.itdrive.model.Room;

public interface RoomRepository extends CrudRepository<Room> {
    Room findByName(String name);
}

package ru.itdrive.repositories;

import java.util.List;

public interface CrudRepository<T> {
    void save(T object);
    void update(T object);
    void delete(T object);
    T find(Integer id);

    List<T> findAll();
}

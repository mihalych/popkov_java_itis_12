package ru.itdrive.repositories;

import ru.itdrive.model.Message;
import ru.itdrive.model.Room;
import ru.itdrive.model.UserLastRoom;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserLastRoomRepositoryImpl implements UserLastRoomRepository {

    //language=SQL
    private static final String SQL_UPDATE_USER_LAST_ROOM = "UPDATE user_last_room SET room_id = ? where user_id = ?";

    //language=SQL
    private static final String SQL_SELECT_USER_LAST_ROOM = "SELECT * FROM user_last_room where user_id = ?";

    //language=SQL
    private static final String SQL_INSERT_USER_LAST_ROOM = "insert into user_last_room(room_id, user_id) values (?, ?)";

    private DataSource dataSource;

    public UserLastRoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<UserLastRoom> userLastRoomRowMapper = new RowMapper<UserLastRoom>() {
        public UserLastRoom mapRow(ResultSet row)  throws SQLException {
            return new UserLastRoom(
                    row.getInt("id"),
                    row.getInt("user_id"),
                    row.getInt("room_id")
            );
        }
    };

    @Override
    public void save(UserLastRoom object) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_USER_LAST_ROOM);
            statement.setInt(1,object.getRoomId());
            statement.setInt(2,object.getUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    @Override
    public void update(UserLastRoom object) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_USER_LAST_ROOM);
            statement.setInt(1,object.getRoomId());
            statement.setInt(2,object.getUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    @Override
    public void delete(UserLastRoom object) {

    }

    @Override
    public UserLastRoom find(Integer id) {
        return null;
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public UserLastRoom findUserLastRoom(Integer userId) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER_LAST_ROOM);
            statement.setInt(1, userId);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return userLastRoomRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }
}

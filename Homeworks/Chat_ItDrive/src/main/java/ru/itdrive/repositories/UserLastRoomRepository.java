package ru.itdrive.repositories;

import ru.itdrive.model.UserLastRoom;

public interface UserLastRoomRepository extends CrudRepository<UserLastRoom> {

    UserLastRoom findUserLastRoom(Integer userId);
}

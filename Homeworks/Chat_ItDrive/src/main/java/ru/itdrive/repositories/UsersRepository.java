package ru.itdrive.repositories;

import ru.itdrive.model.User;

public interface UsersRepository extends CrudRepository<User> {
    User findByFirstName(String firstName);
    User findByEmail(String email);
}

package ru.itdrive.repositories;

import ru.itdrive.model.User;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UsersRepositoryImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from users";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from users where id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_FIRST_NAME = "select * from users where first_name = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "select * from users where email = ?";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into users(first_name, last_name, email, password) values (?, ?, ?, ?)";

    //private Connection connection;
    private DataSource dataSource;

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {
            return new User(
                    row.getInt("id"),
                    row.getString("first_name"),
                    row.getString("last_name"),
                    row.getString("email"),
                    row.getString("password")
            );
        }
    };

    public UsersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public User findByFirstName(String firstName) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            User result = null;
            statement = connection.prepareStatement(SQL_SELECT_BY_FIRST_NAME);
            statement.setString(1, firstName);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            return userRowMapper.mapRow(resultSet);

        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    @Override
    public User findByEmail(String email) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            User result = null;
            statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL);
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            return userRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public void save(User object) {

        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_USER);
            statement.setString(1, object.getFirstName());
            statement.setString(2, object.getLastName());
            statement.setString(3, object.getEmail());
            statement.setString(4, object.getPassword());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public void update(User object) {

    }

    public void delete(User object) {

    }

    public User find(Integer id) {

        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return userRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public List<User> findAll() {

        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            List<User> result = new ArrayList<User>();
            statement = connection.prepareStatement(SQL_SELECT_ALL);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = userRowMapper.mapRow(resultSet);
                result.add(user);
            }
            return result;
        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }
}

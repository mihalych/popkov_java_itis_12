package ru.itdrive.repositories;

import ru.itdrive.model.Message;
import ru.itdrive.model.Room;
import ru.itdrive.model.User;

import java.util.List;

public interface MessagesRepository extends CrudRepository<Message>  {
    List<Message> findMessagesByAuthor(User author);
    List<Message> findMessagesByRoom(Room room);
}

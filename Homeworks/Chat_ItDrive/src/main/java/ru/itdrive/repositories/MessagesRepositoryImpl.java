package ru.itdrive.repositories;

import ru.itdrive.model.Message;
import ru.itdrive.model.Room;
import ru.itdrive.model.User;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class MessagesRepositoryImpl implements MessagesRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from messages";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from messages where id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ROOM = "select * from messages where room_id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_AUTHOR = "select * from messages where author_id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_AUTHOR_ROOM = "select * from messages where author_id = ? and room_id = ?";

    //language=SQL
    private static final String SQL_INSERT_MESSAGE = "insert into messages(room_id, author_id, text, create_date) values (?, ?, ?, ?)";

    private DataSource dataSource;

    private RowMapper<Message> messageRowMapper = new RowMapper<Message>() {
        public Message mapRow(ResultSet row)  throws SQLException {
            return new Message(
                    row.getInt("id"),
                    row.getString("text"),
                    row.getInt("room_id"),
                    row.getInt("author_id"),
                    row.getDate("create_date")
            );
        }
    };

    public MessagesRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void save(Message object) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_MESSAGE);
            statement.setInt(1,object.getChat_id());
            statement.setInt(2,object.getAuthor_id());
            statement.setString(3,object.getText());
            statement.setDate(4,object.getCreate_date());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public void update(Message object) {

    }

    public void delete(Message object) {

    }

    public Message find(Integer id) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return messageRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public List<Message> findAll() {
        PreparedStatement statement = null;
        try {
            List<Message> result = new ArrayList<Message>();
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ALL);

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                Message message = messageRowMapper.mapRow(resultSet);
                result.add(message);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public List<Message> findMessagesByAuthor(User author) {
        PreparedStatement statement = null;
        try {
            List<Message> result = new ArrayList<Message>();
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_AUTHOR);
            statement.setInt(1, author.getId());

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                result.add(messageRowMapper.mapRow(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public List<Message> findMessagesByRoom(Room room) {
        PreparedStatement statement = null;
        try {
            List<Message> result = new ArrayList<Message>();
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_ROOM);
            statement.setInt(1, room.getId());

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                result.add(messageRowMapper.mapRow(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    public List<Message> findMessagesByRoomAndAuthor(Integer roomId, Integer userId) {
        PreparedStatement statement = null;
        try {
            List<Message> result = new ArrayList<Message>();
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_AUTHOR_ROOM);
            statement.setInt(1, userId);
            statement.setInt(2, roomId);

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                result.add(messageRowMapper.mapRow(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }
}

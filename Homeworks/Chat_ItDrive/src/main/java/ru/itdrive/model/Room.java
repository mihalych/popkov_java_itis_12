package ru.itdrive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Data
public class Room {
    private Integer id;
    private String name;
    private Integer chat_id;
}

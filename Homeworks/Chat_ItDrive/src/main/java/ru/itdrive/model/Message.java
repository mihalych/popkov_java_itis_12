package ru.itdrive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Data
@AllArgsConstructor
@Getter
@Setter
public class Message {
    private Integer id;
    private String text;
    private Integer chat_id;
    private Integer author_id;
    private Date create_date;
}

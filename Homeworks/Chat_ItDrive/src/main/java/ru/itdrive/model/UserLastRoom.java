package ru.itdrive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@AllArgsConstructor
@Getter
@Setter
public class UserLastRoom {
    private Integer id;
    private Integer userId;
    private Integer roomId;
}

package ru.itdrive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@AllArgsConstructor
@Getter
@Setter
public class Chat {
    private Integer id;
    private String name;
    private Date create_date;
}

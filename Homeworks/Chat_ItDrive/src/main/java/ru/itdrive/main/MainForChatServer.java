package ru.itdrive.main;

import ru.itdrive.config.ApplicationConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.server.ChatServer;

public class MainForChatServer {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        ChatServer chatServer = context.getBean(ChatServer.class);
        chatServer.start();

    }
}

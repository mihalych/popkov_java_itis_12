package serverSocket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class serverSocket {

    private ArrayList<Socket> socketClientArray = new ArrayList<Socket>();

    public void start(int port) {
        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(port);
            while(true) {
                new EchoClientHandler(serverSocket.accept()).start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class EchoClientHandler extends Thread {
        private Socket socketClient;

        public EchoClientHandler(Socket socket) {
            this.socketClient = socket;
            socketClientArray.add(this.socketClient);
        }

        public void run() {
            try {
                InputStream inputStream = socketClient.getInputStream();
                BufferedReader clientReader = new BufferedReader(new InputStreamReader(inputStream));

                String inputLine = clientReader.readLine();
                while (inputLine != null) {
                    for(int i = 0; i < socketClientArray.size(); i++) {
                        PrintWriter writer = new PrintWriter(socketClientArray.get(i).getOutputStream(), true);
                        writer.println(inputLine);
                    }
                    inputLine = clientReader.readLine();
                }

            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

    }
}

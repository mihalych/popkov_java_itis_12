package ru.itdrive.models;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@Data
@Builder
public class Room {
    private Integer id;
    private String name;
    private Integer users_count;
}

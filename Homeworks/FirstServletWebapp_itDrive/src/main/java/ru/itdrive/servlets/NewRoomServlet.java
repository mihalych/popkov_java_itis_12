package ru.itdrive.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.models.Room;
import ru.itdrive.repositories.RoomRepositoryImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addNewRoom")
public class NewRoomServlet extends HttpServlet {

    RoomRepositoryImpl roomRepository;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("html/addNewRoom.html");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String roomName = req.getParameter("name");
        String usersCountString = req.getParameter("usersCount");
        Integer usersCount = null;
        try {
            usersCount = Integer.valueOf(usersCountString);
        } catch (IllegalArgumentException e) {
            System.err.println("invalid users count");
        }

        if (usersCount != null) {
            Room room = Room.builder()
                    .name(roomName)
                    .users_count(usersCount)
                    .build();

            roomRepository.save(room);
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        roomRepository = context.getBean(RoomRepositoryImpl.class);
    }

}

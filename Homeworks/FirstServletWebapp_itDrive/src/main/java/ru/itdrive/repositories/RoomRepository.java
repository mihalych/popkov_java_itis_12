package ru.itdrive.repositories;

import ru.itdrive.models.Room;

public interface RoomRepository extends CrudRepository<Room> {
    Room findByName(String name);
}

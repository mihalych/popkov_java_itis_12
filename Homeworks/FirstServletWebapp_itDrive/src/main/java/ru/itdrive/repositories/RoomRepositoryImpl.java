package ru.itdrive.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.models.Room;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class RoomRepositoryImpl implements RoomRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select * from rooms where name = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from rooms where id = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from rooms";

    //language=SQL
    private static final String SQL_SAVE = "insert into rooms(name, users_count) values (?, ?)";

    private DataSource dataSource;

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        public Room mapRow(ResultSet row)  throws SQLException {
            return new Room(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getInt("chat_id")
            );
        }
    };

    public RoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Room findByName(String name) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_NAME);
            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            return roomRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }

    @Override
    public void save(Room object) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SAVE);
            statement.setString(1, object.getName());
            statement.setInt(2, object.getUsers_count());
            statement.executeUpdate();
        } catch (SQLException e) {

        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }

    @Override
    public void update(Room object) {

    }

    @Override
    public void delete(Room object) {

    }

    @Override
    public Room find(Integer id) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            return roomRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored){

                }
            }
        }
    }

    @Override
    public List<Room> findAll() {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            List<Room> result = new ArrayList<Room>();
            statement = connection.prepareStatement(SQL_SELECT_ALL);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Room room = roomRowMapper.mapRow(resultSet);
                result.add(room);
            }
            return result;
        } catch (SQLException e) {
            return null;
            //throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }
}

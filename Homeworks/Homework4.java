import java.util.Scanner;

class Homework4 {
	public static void main(String[] args) {
		int number = 0;
		int sum = 0;
		
		while (number != -1) {
			Scanner scanner = new Scanner(System.in);
			number = scanner.nextInt();
			int a = number;
			int multiNumber = 1;
			while (a > 0) {
				multiNumber = multiNumber * a % 10;
				a = a / 10;
			}
			if (multiNumber % 3 == 0) {
				sum += number;
			}
		}
		System.out.println(sum);
	}
}
import java.util.Scanner;

class Homework8 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int resultIndex = scanner.nextInt();
		int a = fibonacci(resultIndex);
		System.out.println(a);
	}

	public static int fibonacci(int n) {
		if (n <= 1) 
            return n; 
        return fibonacci(n - 1) + fibonacci(n - 2);
	}
}
import Homework15.FileScanner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import static org.junit.Assert.*;

public class FileScannerTest {

    private FileScanner scanner;

    @Before
    public void setUp() throws FileNotFoundException {
        this.scanner = new FileScanner("C:\\Projects\\popkov_java_itis_12\\Homeworks\\Homework15\\input.txt");
    }

    @Test
    public void testNextInt() {
        int result = this.scanner.nextInt();
        assertEquals(232343534, result);
        int result1 = this.scanner.nextInt();
        assertEquals(55, result1);
    }

    @Test
    public void testNextLine() {
        String result = this.scanner.nextLine();
        assertEquals("Hello world", result);
        String result1 = this.scanner.nextLine();
        assertEquals("232343534", result1);
    }

}

package Homework15;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class FileScanner {

    private String fileName;

    private FileInputStream file;

    private Scanner scanner;

    public FileScanner(String fileName) throws FileNotFoundException {
        this.fileName = fileName;
        this.file = new FileInputStream(fileName);
        this.scanner = new Scanner(this.file);
    }

    public int nextInt() {

        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                return scanner.nextInt();
            } else {
                scanner.next();
            }
        }
        return 0;
    }

    public String nextLine() {

        while (scanner.hasNext()) {
            if (scanner.hasNextLine()) {
                return scanner.nextLine();
            } else {
                scanner.next();
            }
        }
        return null;
    }

}
